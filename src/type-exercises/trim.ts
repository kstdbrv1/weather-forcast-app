

type Trim<A> = A extends [infer H] ? H : never;

type Trimmed = Trim<'  Hello World  '>


export {}