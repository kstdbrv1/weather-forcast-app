export interface IPastCardState {
  unixDate: null | number
  cityLocation: null | string
};