import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './src/App';

test('renders forecast', () => {
  render(<App />);
  const element = screen.getByText(/forecast/i);
  expect(element).not.toBeInTheDocument();
});

